executableName = ""
sourceNames = []

if (ARGV.length  != 1)
	puts "Provide a single executable as an argument"
	exit
end

if File.exist?(ARGV[0])
	executableName = ARGV[0]
elsif 
	puts "The executable does not exist"
	exit	
end



#================================================================================================================================================#
#============================================GENERATE OBJDUMP and LLVM===========================================================================#
if !system("objdump -d "+executableName+" > "+executableName+".o")
	puts "could not generate objdump"
	exit
end 

if !system("llvm-dwarfdump --debug-line "+executableName+" > "+executableName+".l")
	puts "could not generate llvm-dwarfdump"
	exit
end


#================================================================================================================================================#
#============================================READING OBJDUMP and LLVM============================================================================#
#Variables to read .l file
file_names_index, name, dir_index, mod_time, length = 0,1,2,3,4
address, line, column, file, isa, discriminator  = 0,1,2,3,4,5
$llvmHash = {}  	#global llvmhash hashmap {key, value} -> {filename.c, hashmap of lines with addresses}
$objdump = {}		#global objdump file
$allFileLists = {} 	#global all files lists hashmap {key, value} -> {filename.c, hashmap of all other associated files}
$LineArrayHash = {}     #global hashmap with all the line numbers with filename.c as the key


$functionlist = {}
$functionlistLineNum = {}
$lengthAddress = 0


#============================================Reading the LLVM=========================================================# 
llvmFile = File.read(executableName+".l")

#===== matching large sections per source files in LLVM
llvmFile.scan(/debug_line\[[a-z0-9]*\](.*?)end_sequence/m) { |matchedString| 
  tempfileList = {}

  #===== matching files in LLVM
  matchedString.to_s.scan(/file_names\[\s+(?<file_names_index>\d+)\]:\\n\s+name: \\"(?<name>\S+)\\"\\n\s+dir_index: (?<dir_index>\d+)\\n\s+mod_time: (?<mod_time>0x\d+)\\n\s+length: (?<length>0x\d+)/m){ |matchFileName|
  	contentArray = [matchFileName[file_names_index], matchFileName[name], matchFileName[dir_index], matchFileName[mod_time], matchFileName[length]]
  	tempfileList[matchFileName[file_names_index]] = contentArray
  }
  $allFileLists[tempfileList.values[file_names_index][name]] = tempfileList
  
  #===== matching lines and addresses in LLVM
  templlvmHash = {}
  tempLineArray = []
  matchedString.to_s.scan(/(?<address>0[xX][0-9a-fA-F]+)\s+(?<line>\d+)\s*(?<column>\d+)\s*(?<file>\d+)\s*(?<isa>\d+)\s*(?<discriminator>\d+)/) { |matchAddress| 
  	contentArray = [matchAddress[address], matchAddress[line], matchAddress[column], matchAddress[file], matchAddress[isa], matchAddress[discriminator]]
  	templlvmHash[matchAddress[line]] = contentArray
	tempLineArray.push(matchAddress[line])
  }
  $llvmHash[tempfileList.values[file_names_index][name]] = templlvmHash
  $LineArrayHash[tempfileList.values[file_names_index][name]] = tempLineArray

  
}

$lengthAddress = $llvmHash.values[0].values[0][address].length


#============================================Reading the OBJDUMP=========================================================#
#Reading .o file
assembly_address, byte_code, operator, operands = 0,1,2,3
function_address, function_name = 0,1

objFile = File.read(executableName+".o")

#===== storing objdump line by line
$objLineNum = 0
$objFilelines = []

File.open(executableName+".o").each do |line|
	$objLineNum += 1
	$objFilelines[$objLineNum] = line
end

#===== matching address with assembly code
objFile.scan(/(?<assembly_address>\d+\S*\d*):\s(?<byte_code>[a-z0-9 ]*)\t*(?<operator>\S*)(?<operands>.*)[\r\n]+/) { |matchedString| 
  #getting the line number
  if (matchedString[operator] == '')
	str = "  "+ matchedString[assembly_address] + ":	" + matchedString[byte_code] + "\n"
  else 
  	str = "  "+ matchedString[assembly_address] + ":	" + matchedString[byte_code] + "	" + matchedString[operator] + matchedString[operands] + "\n"
  end
  assem_lineNum = $objFilelines.find_index(str)
  #key is the assembly address
  zeroPad = "0" * ($lengthAddress - matchedString[assembly_address].length - 2)
  matchedString[assembly_address].prepend("0x", zeroPad)
  contentArray = [matchedString[assembly_address], matchedString[byte_code], matchedString[operator], matchedString[operands], assem_lineNum]
  $objdump[matchedString[assembly_address]] = contentArray
  
}
$objdumpKeys = $objdump.keys

#===== matching address with function name
objFile.scan(/(?<function_address>[a-z0-9]+) <(?<function_name>\S+)>:/) { |matchedString| 
  # Getting line number 
  str = matchedString[function_address] + " <" + matchedString[function_name] + ">:\n"
  func_lineNum = $objFilelines.find_index(str)
  #key is the file names index
  matchedString[function_address].prepend("0x")
  contentArray = [matchedString[function_address], matchedString[function_name], func_lineNum]
  $functionlist[matchedString[function_address]] = contentArray
  $functionlistLineNum[func_lineNum] = contentArray
}


#================================================================================================================================================#
#============================================ALGORITHIM TO MATCH LINE AND ASSEM==================================================================#


def genHTML(sourceFile, sourceFileName, amtLines, minAssemLine, maxAssemLine, b_getMin)
   stringHTML = ""
   tempIndex = 0
   
   $LineArrayHash[sourceFileName].each_with_index do |lineNum, index|
	lineAssem = getAsemForLine(lineNum, index, sourceFileName)
	stringHTML += "<div class=\"code-block\">"
	temphref = ""

	
	#=====Generate asm-block
	asmHTML = "<div class=\"asm-block\">
        	  	<span>~</span>"
        		
	b_hasLineNum = false
	for assem in lineAssem
		temp = assem[0]
		if (!b_hasLineNum)
			asmHTML += "<pre class=\"prettyprint linenums:" + temp[4].to_s + " \">"
			b_hasLineNum = true
		end
		if(temphref == "")
			temphref = isControlOp(temp) # return address if function exists, else false
		end			
		asmHTML += "<span id="+temp[0].to_s+"></span>"
		tempAssem = $objFilelines[temp[4]]
		tempAssem.gsub!("<", "&lt")
		tempAssem.gsub!(">", "&gt")
		
		asmHTML += tempAssem
		##===================
		if(temp[4]<minAssemLine && b_getMin)
			minAssemLine = temp[4]
			b_getMin = false;

		end

		if(temp[4]>maxAssemLine)
			maxAssemLine = temp[4]

		end
	end
	asmHTML += "</pre></div>" #close asm-block


	#=====Generate src-block


        if(temphref != "")
		temphref = "href=\"#"+temphref.to_s+"\""
	end


	srcHTML = "<div class=\"src-block\">
		        <span>"+sourceFileName+"</span>
		        <pre class=\"prettyprint linenums:"+(tempIndex+1).to_s+"\"><a "+temphref.to_s+" >"
	tempSrc = ""
	if(lineNum != tempIndex)
		for i in (tempIndex+1)..lineNum.to_i
			tempSrc = sourceFile[i].to_s
			tempSrc.gsub!("<", "&lt")
			tempSrc.gsub!(">", "&gt")
			srcHTML += tempSrc
		end 
		tempIndex = lineNum.to_i
	end
	srcHTML += "</a></pre></div>"

	
	#=====Join asm and source html and place into string html
	stringHTML += asmHTML 
	stringHTML += srcHTML

	stringHTML += "</div>" #close code-block

   end
   return [stringHTML, minAssemLine, maxAssemLine, b_getMin]
end


def getAsemForLine(lineNum, index, sourceFileName)
   resultAsem = []
   #add a method here to get the addresses for a line
   if !($llvmHash[sourceFileName].has_key?(lineNum.to_s))
	return resultAsem = []
   end

   lineArray = $LineArrayHash[sourceFileName]

   if lineArray[index+1] #check to see if the index exist
	 endLine = lineArray[index+1] 
   elsif
	resultAsem.push([$objdump[$objdumpKeys[index]], "false"])
	return resultAsem
   end


   beginAddress =  $llvmHash[sourceFileName][lineNum][0]
   endAddress = $llvmHash[sourceFileName][endLine][0]


   startIndex = $objdump.find_index {|key,| key == beginAddress}
   endIndex = $objdump.find_index {|key,| key == endAddress}


   
   for currentIndex in startIndex..endIndex
	if(currentIndex == endIndex)
    		break
        end

	if ($functionlist.has_key?($objdumpKeys[currentIndex]))
		resultAsem.push([$objdump[$objdumpKeys[currentIndex]], "true"]) #true means that it links to a function
	elsif
		resultAsem.push([$objdump[$objdumpKeys[currentIndex]], "false"])
	end
   end
   return resultAsem
end


#===================Function to check operator=============#
def isControlOp(assembly)
	href = ""
	assemOperator = assembly[2].to_s

	if(assemOperator.start_with?('call', 'j', 'ret'))		# Is Operator a control flow
		assemOperand = assembly[3].to_s.strip.split(" ")[0]
		if(assemOperand.to_s != '')				# Is there an operand					
			# Padding
			zeroPad = "0" * ($lengthAddress - assemOperand.length - 2)
			assemOperand.prepend("0x", zeroPad)
			# Does it exit in the function list, if yes returns address
			if ($functionlist.has_key?(assemOperand))
				href = assemOperand
				return href				
			end
		end
	end
	return href
end

#=============================================================HTML CODE GENERATION===================================================================================#
stringHTML = "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>xref for binary: {{binary_name}}</title>

    <script src=\"https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js\"></script>
    <style>
        .code-block {
            display: table;
            box-sizing: border-box;
            position: relative;
            table-layout: fixed;
            width: 100%;
        }
        pre {
            margin: 0;
            overflow: auto;
            white-space: pre-wrap;
            word-wrap: break-word;
        }
        .asm-block,
        .src-block {
            width: 50%;
            display: table-cell;
            vertical-align: top;
        }
        li.L0, li.L1, li.L2, li.L3,
        li.L5, li.L6, li.L7, li.L8 {
            list-style-type: decimal !important;
        }

	/* Pretty printing styles. Used with prettify.js. */
	/* Vim sunburst theme by David Leibovic */

	pre .str, code .str { color: #65B042; } /* string  - green */
	pre .kwd, code .kwd { color: #E28964; } /* keyword - dark pink */
	pre .com, code .com { color: #AEAEAE; font-style: italic; } /* comment - gray */
	pre .typ, code .typ { color: #89bdff; } /* type - light blue */
	pre .lit, code .lit { color: #3387CC; } /* literal - blue */
	pre .pun, code .pun { color: #fff; } /* punctuation - white */
	pre .pln, code .pln { color: #fff; } /* plaintext - white */
	pre .tag, code .tag { color: #89bdff; } /* html/xml tag    - light blue */
	pre .atn, code .atn { color: #bdb76b; } /* html/xml attribute name  - khaki */
	pre .atv, code .atv { color: #65B042; } /* html/xml attribute value - green */
	pre .dec, code .dec { color: #3387CC; } /* decimal - blue */

	pre.prettyprint, code.prettyprint {
		background-color: #000;
		border-radius: 8px;
	}

	pre.prettyprint {
		width: 95%;
		margin: 1em auto;
		padding: 1em;
		white-space: pre-wrap;
	}


	/* Specify class=linenums on a pre to get line numbering */
	ol.linenums { margin-top: 0; margin-bottom: 0; color: #AEAEAE; } /* IE indents via margin-left */
	li.L0,li.L1,li.L2,li.L3,li.L5,li.L6,li.L7,li.L8 { list-style-type: none }
	/* Alternate shading for lines */
	li.L1,li.L3,li.L5,li.L7,li.L9 { }

	@media print {
	  pre .str, code .str { color: #060; }
	  pre .kwd, code .kwd { color: #006; font-weight: bold; }
	  pre .com, code .com { color: #600; font-style: italic; }
	  pre .typ, code .typ { color: #404; font-weight: bold; }
	  pre .lit, code .lit { color: #044; }
	  pre .pun, code .pun { color: #440; }
	  pre .pln, code .pln { color: #000; }
	  pre .tag, code .tag { color: #006; font-weight: bold; }
	  pre .atn, code .atn { color: #404; }
	  pre .atv, code .atv { color: #060; }
	}


	li.L1, li.L3, li.L5, li.L7, li.L9 {
	    background: #202020;
	}

	body {
	    background-color: rgb(230, 230, 230)
	}

	#myInput {
	  background-image: url('/css/searchicon.png'); /* Add a search icon to input */
	  background-position: 10px 12px; /* Position the search icon */
	  background-repeat: no-repeat; /* Do not repeat the icon image */
	  width: 100%; /* Full-width */
	  font-size: 16px; /* Increase font-size */
	  padding: 12px 20px 12px 40px; /* Add some padding */
	  border: 1px solid #ddd; /* Add a grey border */
	  margin-bottom: 12px; /* Add some space below the input */
	}

	#myUL {
	  /* Remove default list styling */
	  list-style-type: none;
	  padding: 0;
	  margin: 0;
	}

	#myUL li a {
	  border: 1px solid #ddd; /* Add a border to all links */
	  margin-top: -1px; /* Prevent double borders */
	  background-color: #f6f6f6; /* Grey background color */
	  padding: 12px; /* Add some padding */
	  text-decoration: none; /* Remove default text underline */
	  font-size: 18px; /* Increase the font-size */
	  color: black; /* Add a black text color */
	  display: block; /* Make it into a block element to fill the whole list */
	}

	#myUL li a:hover:not(.header) {
	  background-color: #eee; /* Add a hover effect to all links, except for headers */
	}

</head>
<body>
</style>


<center><h1>Cross Indexer</h1></center>"
max = 0
min = 1.0/0
sourceAssemHTML = ""
b_getMin = true
for sourceFileName in $allFileLists.keys
	sourceFile = []
        lineNums = 0

        if File.exist?(sourceFileName)
		#===Open file and create a  hashmap
		File.open(sourceFileName).each do |line|
			lineNums += 1
			sourceFile[lineNums] = line
		end
	elsif 
		puts "The source file does not exist"
		exit	
	end
	returnedGenArray = genHTML(sourceFile, sourceFileName, lineNums, min, max, b_getMin)
	sourceAssemHTML += returnedGenArray[0]
	min = returnedGenArray[1]
	max = returnedGenArray[2]
	b_getMin = returnedGenArray[3]
end

aboveAssemHTML = ""
aboveAssemHTML += "<div class=\"code-block\">
		<div class=\"asm-block\">
	  	<span>~</span>
		<pre class=\"prettyprint linenums: 1 \">"
for i in 1..(min-1)
	if ($functionlistLineNum[i])
		aboveAssemHTML += "<span id="+$functionlistLineNum[i][0]+"></span>"
	end
	temp = $objFilelines[i]
	temp.gsub!("<", "&lt")
	temp.gsub!(">", "&gt")
	temp.gsub!("\n", "<br>")
	#aboveAssemHTML += "<span id="+temp.to_s+"></span>"
	aboveAssemHTML += temp

end
aboveAssemHTML += "</pre></div><div class=\"src-block\"></div></div>" #close asm-block

belowAssemHTML = ""
belowAssemHTML += "<div class=\"code-block\">
		<div class=\"asm-block\">
	  	<span>~</span>
		<pre class=\"prettyprint linenums:"+ (max+1).to_s+ " \">"
for i in (max+1)..$objLineNum
	if ($functionlistLineNum[i])
		belowAssemHTML += "<span id="+$functionlistLineNum[i][0]+"></span>"
	end
	temp = $objFilelines[i]
	temp.gsub!("<", "&lt")
	temp.gsub!(">", "&gt")

	belowAssemHTML += temp

end
belowAssemHTML += "</pre></div><div class=\"src-block\"></div></div>" #close asm-block


stringHTML += aboveAssemHTML
stringHTML += sourceAssemHTML
stringHTML += belowAssemHTML
stringHTML += "
</body>
</html>"

File.open("HTML/index.html", "w") {|f| f.write(stringHTML) }


