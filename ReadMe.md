# Cross-Indexer

Is a scripting program called xref that uses the output of llvm-dwarfdump and objdump ‑d to construct a web page that contains side-by-side assembly language and corresponding source code for a given program.  The assembly and source code lines up nicely on the page: the first instruction in each contiguous block of instructions that come from the same line of source is horizontally aligned with a copy of that source line.  Source lines without corresponding assembly instructions (e.g., comments and declarations) are presented immediately above the first occurrence of the following source line, or at the bottom of the page if there are no more lines with corresponding assembly code.  In the rare case that a single assembly instruction is generated for multiple lines of source, the instruction lines up with the final source line.  Source for in-lined functions are displayed in-line, even if it comes from a different source file than do the surrounding lines. 

## Getting Started


```
1. compile your program using:
    - gcc -g3 myfile1.c myfile2.c ... -o {executableName}
2. Once you have the executable, run the command:
    - ruby xref.rb <executableName>
3. The xref program will generate an index.html file in your HTML directory
    - HTML/index.html (Open the page with-in your browser)
```

## Sample Program

```
1. gcc -g3 test.c -o test
2. ruby xref.rb test
3. Open HTML/index.html
```

### Prerequisites


```
1. Install Ruby https://www.ruby-lang.org/en/documentation/installation/
2. Install gcc
3. Install objdump
4. Install llvm-dwarfdump 
```

### Supported Features


```
1. supports additional cross-references: function names
2. support additional programming language: Supports all C-like, Bash-like, and XML-like languages
3. Syntax-color the code in your pages, so that comments, declarations, keywords, constants, etc., are visibly distinguished. 
```

### Ouput HTML Page
![Cross Indexer](https://lh3.googleusercontent.com/XMHn9Qv2yDxiuZIGaFshqsNYqY1qM9an3K5D40cBm6HvaXH4SRBXw_3VtNmDqODKpSc_vqvScI2Upy7OzZttTTzYZYg4uLzLBjf1AVJzfePnFNQ_ejKCmUjCV0wnLjvDVul26L1ehdcLt7PSlk3pqSqipoJ37YI3UtDHVIUFS-cKOu2O0uTZWJsYq4fWOwb8lspKv41UzGd6bXkSCLv60x75Kh2SjptIGb2q66ERqdGfQ811Bhff865STT-SZNiiFcE5_WBNfJg7RI5VGzWuU5CqOgHkDbAPlUULt5h-q6E0z5jh8XwCfubDV-NRrjA8xGKjoHN1iyvEHndOyxSCMP4Z61HpnCSgSu-2Y4JGq20ZaNRIc-8ZEuJMTQOLlLKR08rg-065flBHQsA4IeVXiMQeX4mOijkLYGxk5M_3MRyPEwXluCSgWBamkdpjMbnpQw9jIc5dzkQ-7gLmWcy3vD_mCLMjoP64G9sc3lkdMppwiNCWsbNKqdh7eAt9UTKyeNMtJ-IpqmfHUtlnPyh0Miz6rTvfmcwH5xgFwWECv1XQ41Yb3VCc84mmYReUpTI_MzogIJGaFLYP3UrYhO0ABsuhtrtyXrZi00tvCqqAtVbJIebKan8tG9MlNA=w3838-h2055)
